/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fernandez_sanchez_abraham_PROG09_Tarea;

import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import negocio.CuentaAhorroClass;
import negocio.CuentaClass;
import negocio.CuentaCorrienteEmpresaClass;
import negocio.CuentaCorrientePersonalClass;
import negocio.PersonaClass;
import negocio.UtilClass;

/**
 *
 * @author Abraham Fernandez Sanchez
 */
public final class VistaNuevaCuenta extends javax.swing.JInternalFrame {

    PersonaClass persona;
    CuentaClass cuenta;
    UtilClass util;
    CuentaAhorroClass cuentaAhorro;
    CuentaCorrientePersonalClass CCPersonal;
    CuentaCorrienteEmpresaClass CCEmpresa;

    // Declaracion de variables    
    private String nombre;
    private String apellidos;
    private String fechaNacimiento;
    private String dni;
 
    private String tipoCuenta;
    private Double saldo;
    private String numeroCuenta;
    private Double tipoInteresAnual;
    private ArrayList<String> listaEntidades;
    private Double comisionMantenimiento;
    private Double tipoInteresDescubierto;
    private Double maximoDescubiertoPermitido;
    private Double comisionFijaDescubierto;

    //constantes generales de tipo de cuentas    
    static private final String CUENTA_AHORRO = "CUENTA_AHORRO";
    static private final String CUENTA_CORRIENTE_PERSONAL = "CUENTA_CORRIENTE_PERSONAL";
    static private final String CUENTA_CORRIENTE_EMPRESA = "CUENTA_CORRIENTE_EMPRESA";
    static private String tipoCuentaSeleccionada;

    /**
     * Creates new form VistaNuevaCuenta
     */
    public VistaNuevaCuenta() {
        initComponents();
        vistaInicial();
        grupoRadioButton();
    }

    /**
     * Metodo para crear un selector tipo radio y a�adir sus diferentes items de
     * selecion
     */
    public void grupoRadioButton() {
        buttonGroupCuentasCorrientes.add(jRadioButtonCAhorro);
        buttonGroupCuentasCorrientes.add(jRadioButtonCCPersona);
        buttonGroupCuentasCorrientes.add(jRadioButtonCCEmpresa);
    }

    /**
     * Metodo donde se indican los parametros iniciales de esta ventana
     */
    public void vistaInicial() {
//        [325, 475] tama�o con todas las opciones visibles
        //panel con todos los tipos invisible, hasta que seleccione una opcion del radio button
        jPanelTipos.setVisible(false);
        jPanelCAhorro.setVisible(false);
        jPanelCCPersonal.setVisible(false);
        jPanelCCEmpresa.setVisible(false);
        //tama�o sin tener seleccionado ningun tipo de cuenta
        this.setSize(325, 250);
        //hago la ventana fija
        this.setResizable(false);
    }

    /**
     * Metodo que borrara el contenido de todos los campos de esta vista
     */
    public void clearSelection() {
        jTextFieldNombre.setText("");
        jTextFieldApellidos.setText("");
        jTextFieldDni.setText("");
        jTextFieldFnacimiento.setText("");
        jTextFieldSaldoInicial.setText("");
        buttonGroupCuentasCorrientes.clearSelection();// deselecciona los jbuttons
        vistaInicial();
        jTextFieldNCuenta.setText("");
        jTextFieldTIRemuneracion.setText("");
        jTextFieldTIMantenimiento.setText("");
        jTextFieldMaximoDescubierto.setText("");
        jTextFieldInteresDescubierto.setText("");
        jTextFieldComisionFijaDescubierto.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupCuentasCorrientes = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldFnacimiento = new javax.swing.JTextField();
        jTextFieldDni = new javax.swing.JTextField();
        jTextFieldApellidos = new javax.swing.JTextField();
        jTextFieldNombre = new javax.swing.JTextField();
        jRadioButtonCAhorro = new javax.swing.JRadioButton();
        jRadioButtonCCPersona = new javax.swing.JRadioButton();
        jRadioButtonCCEmpresa = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldSaldoInicial = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jPanelTipos = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jTextFieldNCuenta = new javax.swing.JTextField();
        jPanelCAhorro = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jTextFieldTIRemuneracion = new javax.swing.JTextField();
        jPanelCCPersonal = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jTextFieldTIMantenimiento = new javax.swing.JTextField();
        jPanelCCEmpresa = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jTextFieldInteresDescubierto = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldMaximoDescubierto = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextFieldComisionFijaDescubierto = new javax.swing.JTextField();
        jButtonCrearCuenta = new javax.swing.JButton();
        jButtonReset = new javax.swing.JButton();
        jButtonCancelar = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setPreferredSize(new java.awt.Dimension(325, 475));

        jLabel1.setText("Nombre :");

        jLabel2.setText("Apellidos :");

        jLabel3.setText("DNI :");

        jLabel4.setText("F. Nacimiento :");

        buttonGroupCuentasCorrientes.add(jRadioButtonCAhorro);
        jRadioButtonCAhorro.setText("C. Ahorro");
        jRadioButtonCAhorro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonCAhorroActionPerformed(evt);
            }
        });

        buttonGroupCuentasCorrientes.add(jRadioButtonCCPersona);
        jRadioButtonCCPersona.setText("C.C. Personal");
        jRadioButtonCCPersona.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonCCPersonaActionPerformed(evt);
            }
        });

        buttonGroupCuentasCorrientes.add(jRadioButtonCCEmpresa);
        jRadioButtonCCEmpresa.setText("C.C. Empresa");
        jRadioButtonCCEmpresa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonCCEmpresaActionPerformed(evt);
            }
        });

        jLabel5.setText("Saldo Inicial : ");

        jLabel12.setText("N. Cuenta :");

        jPanelCAhorro.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel7.setText("Tipo Interes Anual :");

        javax.swing.GroupLayout jPanelCAhorroLayout = new javax.swing.GroupLayout(jPanelCAhorro);
        jPanelCAhorro.setLayout(jPanelCAhorroLayout);
        jPanelCAhorroLayout.setHorizontalGroup(
            jPanelCAhorroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCAhorroLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jTextFieldTIRemuneracion, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanelCAhorroLayout.setVerticalGroup(
            jPanelCAhorroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCAhorroLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelCAhorroLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jTextFieldTIRemuneracion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanelCCPersonal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel8.setText("Tipo Interes de mantenimiento :");

        javax.swing.GroupLayout jPanelCCPersonalLayout = new javax.swing.GroupLayout(jPanelCCPersonal);
        jPanelCCPersonal.setLayout(jPanelCCPersonalLayout);
        jPanelCCPersonalLayout.setHorizontalGroup(
            jPanelCCPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCCPersonalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldTIMantenimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanelCCPersonalLayout.setVerticalGroup(
            jPanelCCPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCCPersonalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelCCPersonalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jTextFieldTIMantenimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanelCCEmpresa.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel9.setText("Tipo Interes por descubierto :");

        jLabel10.setText("Maximo descubierto permitido : ");

        jLabel11.setText("Comision fijada por descubierto : ");

        javax.swing.GroupLayout jPanelCCEmpresaLayout = new javax.swing.GroupLayout(jPanelCCEmpresa);
        jPanelCCEmpresa.setLayout(jPanelCCEmpresaLayout);
        jPanelCCEmpresaLayout.setHorizontalGroup(
            jPanelCCEmpresaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCCEmpresaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelCCEmpresaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelCCEmpresaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldMaximoDescubierto)
                    .addComponent(jTextFieldInteresDescubierto, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTextFieldComisionFijaDescubierto, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jPanelCCEmpresaLayout.setVerticalGroup(
            jPanelCCEmpresaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCCEmpresaLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelCCEmpresaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jTextFieldMaximoDescubierto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelCCEmpresaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldInteresDescubierto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelCCEmpresaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldComisionFijaDescubierto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jButtonCrearCuenta.setText("Crear Cuenta");
        jButtonCrearCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCrearCuentaActionPerformed(evt);
            }
        });

        jButtonReset.setText("Reset");
        jButtonReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonResetActionPerformed(evt);
            }
        });

        jButtonCancelar.setText("Cancelar");
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelTiposLayout = new javax.swing.GroupLayout(jPanelTipos);
        jPanelTipos.setLayout(jPanelTiposLayout);
        jPanelTiposLayout.setHorizontalGroup(
            jPanelTiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTiposLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelTiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelCAhorro, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelCCPersonal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanelTiposLayout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldNCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jPanelCCEmpresa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelTiposLayout.createSequentialGroup()
                        .addComponent(jButtonCrearCuenta)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonReset, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonCancelar)))
                .addContainerGap())
        );
        jPanelTiposLayout.setVerticalGroup(
            jPanelTiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTiposLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanelTiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jTextFieldNCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelCAhorro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelCCPersonal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelCCEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanelTiposLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonCrearCuenta)
                    .addComponent(jButtonCancelar)
                    .addComponent(jButtonReset))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanelTipos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel4)
                                            .addComponent(jLabel1)
                                            .addComponent(jLabel2)
                                            .addComponent(jLabel3)
                                            .addComponent(jLabel5))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(jTextFieldSaldoInicial, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextFieldFnacimiento, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                                            .addComponent(jTextFieldDni, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextFieldApellidos, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jTextFieldNombre, javax.swing.GroupLayout.Alignment.LEADING)))
                                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jRadioButtonCAhorro)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jRadioButtonCCPersona)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jRadioButtonCCEmpresa)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextFieldNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextFieldApellidos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3)
                    .addComponent(jTextFieldDni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jTextFieldFnacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldSaldoInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jRadioButtonCAhorro, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jRadioButtonCCEmpresa, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jRadioButtonCCPersona, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanelTipos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jRadioButtonCAhorroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonCAhorroActionPerformed
        jPanelTipos.setVisible(true);
        jPanelCAhorro.setVisible(true);
        jPanelCCPersonal.setVisible(false);
        jPanelCCEmpresa.setVisible(false);
        this.setSize(325, 390);
        tipoCuentaSeleccionada = CUENTA_AHORRO;
    }//GEN-LAST:event_jRadioButtonCAhorroActionPerformed

    private void jRadioButtonCCPersonaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonCCPersonaActionPerformed
        jPanelTipos.setVisible(true);
        jPanelCAhorro.setVisible(false);
        jPanelCCPersonal.setVisible(true);
        jPanelCCEmpresa.setVisible(false);
        this.setSize(325, 390);
        tipoCuentaSeleccionada = CUENTA_CORRIENTE_PERSONAL;
    }//GEN-LAST:event_jRadioButtonCCPersonaActionPerformed

    private void jRadioButtonCCEmpresaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonCCEmpresaActionPerformed
        jPanelTipos.setVisible(true);
        jPanelCAhorro.setVisible(false);
        jPanelCCPersonal.setVisible(false);
        jPanelCCEmpresa.setVisible(true);
        this.setSize(325, 460);
        tipoCuentaSeleccionada = CUENTA_CORRIENTE_EMPRESA;
    }//GEN-LAST:event_jRadioButtonCCEmpresaActionPerformed

    private void jButtonResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonResetActionPerformed
        clearSelection();
    }//GEN-LAST:event_jButtonResetActionPerformed

    private void jButtonCrearCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCrearCuentaActionPerformed
        creacionNuevaCuenta();
        //0=SI - 1=NO 
        String mensaje = "�Quieres crear una nueva cuenta?";
        int respuesta = JOptionPane.showConfirmDialog(null, mensaje, "Confirmaci�n", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);
        if (respuesta == 1) {
            try {
                this.setClosed(true);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(VistaNuevaCuenta.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        clearSelection();
    }//GEN-LAST:event_jButtonCrearCuentaActionPerformed

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        //0=SI - 1=NO 
        String mensaje = "�Realmente quieres salir?";
        int respuesta = JOptionPane.showConfirmDialog(null, mensaje, "Confirmaci�n", JOptionPane.YES_NO_OPTION);
        if (respuesta == 0) {
            try {
                this.setClosed(true);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(VistaNuevaCuenta.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    /**
     * Metodo para la creacion de nuevas cuentas
     */
    private void creacionNuevaCuenta() {
        util = new UtilClass();
        nombre = jTextFieldNombre.getText();
        apellidos = jTextFieldApellidos.getText();
        fechaNacimiento = jTextFieldFnacimiento.getText();
        dni = jTextFieldDni.getText();
        persona = new PersonaClass(nombre, apellidos, fechaNacimiento, dni);
        try {
            numeroCuenta = jTextFieldNCuenta.getText();
            tipoCuenta = tipoCuentaSeleccionada;
            saldo = Double.parseDouble(jTextFieldSaldoInicial.getText());
            listaEntidades = util.entidadesAutorizadas();
            if (!util.validacionNumeroCuentaCorriente(numeroCuenta)){
                JOptionPane.showMessageDialog(this, "El numero de cuenta introducido es incorrecto");
                return;
            }
            seleccionCreacionCuenta(tipoCuentaSeleccionada);  
        } catch (NumberFormatException e) {
            confirmacionTipoDatosNumericos();
        }
        clearSelection();
        showdatos();
    }

    /**
     * Metodo que mostrar un mensaje con los campos exclusivamente numericos en
     * los que se han introducido otro tipo de caracteres
     */
    private void confirmacionTipoDatosNumericos() {
        util = new UtilClass();
        String textoFinal = "Los siguientes campos no han sido rellenados exclusivamente con digitos: ";
        String camposIncorrectos = " ";
        String saldoInicial = jTextFieldSaldoInicial.getText();
        String intAnual = jTextFieldTIRemuneracion.getText();
        String intMantenimiento = jTextFieldTIMantenimiento.getText();
        String maxDescu = jTextFieldMaximoDescubierto.getText();
        String intDescu = jTextFieldInteresDescubierto.getText();
        String comFijaDescu = jTextFieldComisionFijaDescubierto.getText();
        if (!saldoInicial.isEmpty() && !util.isNumeric(saldoInicial)) {
            camposIncorrectos = camposIncorrectos + "\n - Saldo Inicial";
        }
        if (!intAnual.isEmpty() && !util.isNumeric(intAnual)) {
            camposIncorrectos = camposIncorrectos + "\n - Tipo Interes Anual";
        }
        if (!intMantenimiento.isEmpty() && !util.isNumeric(intMantenimiento)) {
            camposIncorrectos = camposIncorrectos + "\n - Tipo Interes de mantenimiento";
        }
        if (!maxDescu.isEmpty() && !util.isNumeric(maxDescu)) {
            camposIncorrectos = camposIncorrectos + "\n - Maximo descubierto permitido";
        }
        if (!intDescu.isEmpty() && !util.isNumeric(intDescu)) {
            camposIncorrectos = camposIncorrectos + "\n - Tipo Interes por descubierto";
        }
        if (!comFijaDescu.isEmpty() && !util.isNumeric(comFijaDescu)) {
            camposIncorrectos = camposIncorrectos + "\n - Comision fijada por descubierto";
        }
        JOptionPane.showMessageDialog(this, textoFinal + camposIncorrectos);
    }

    /**
     * Metodo que mediante un switch se a�adira un tipo de objeto dioferente
     * dependiendo del tipo de cuenta que sea seleccionada
     *
     * @param tipoCuentaSeleccionada String Tipo de cuenta corriente
     */
    private void seleccionCreacionCuenta(String tipoCuentaSeleccionada) {
        switch (tipoCuentaSeleccionada) {
            case CUENTA_AHORRO:
                tipoInteresAnual = Double.parseDouble(jTextFieldTIRemuneracion.getText());
                cuentaAhorro = new CuentaAhorroClass(tipoInteresAnual, persona, tipoCuenta, saldo, numeroCuenta);
                util.addNewCuenta(cuentaAhorro);
                JOptionPane.showMessageDialog(this, "La cuenta se ha creado correctamente");
                break;
            case CUENTA_CORRIENTE_PERSONAL:
                comisionMantenimiento = Double.parseDouble(jTextFieldTIMantenimiento.getText());
                CCPersonal = new CuentaCorrientePersonalClass(comisionMantenimiento, listaEntidades, persona, tipoCuenta, saldo, numeroCuenta);
                util.addNewCuenta(CCPersonal);
                JOptionPane.showMessageDialog(this, "La cuenta se ha creado correctamente");
                break;
            case CUENTA_CORRIENTE_EMPRESA:
                maximoDescubiertoPermitido = Double.parseDouble(jTextFieldMaximoDescubierto.getText());
                tipoInteresDescubierto = Double.parseDouble(jTextFieldInteresDescubierto.getText());
                comisionFijaDescubierto = Double.parseDouble(jTextFieldComisionFijaDescubierto.getText());
                CCEmpresa = new CuentaCorrienteEmpresaClass(tipoInteresDescubierto, maximoDescubiertoPermitido, comisionFijaDescubierto, listaEntidades, persona, tipoCuenta, saldo, numeroCuenta);
                util.addNewCuenta(CCEmpresa);
                JOptionPane.showMessageDialog(this, "La cuenta se ha creado correctamente");
                break;
            default:
                cuenta = new CuentaClass();
                JOptionPane.showMessageDialog(this, "Ha ocurrido un error y se ha creado una cuenta en blanco");
                break;
        } 
    }

    /**
     * Metodo muestra por consola PRUEBAS
     */
    public void showdatos() {
//        util = new UtilClass();
//        int totalCuentas = util.getSizeArrayCuentas();
//
//        for (int i = 0; i < totalCuentas; i++) {
//            cuenta = (CuentaClass) util.getCuentaOfArrayList(i);
//            persona = cuenta.getTitular();
//
//            System.out.println("Nombre: " + persona.getNombre());
//            System.out.println("Apellidos: " + persona.getApellidos());
//            System.out.println("DNI: " + persona.getDni());
//            System.out.println("F. Nacimiento: " + persona.getFechaNacimiento());
//            System.out.println();
//            System.out.println("Tipo de Cuenta: " + cuenta.getTipoCuenta());
//            System.out.println("CCC: " + cuenta.getNumeroCuenta());
//            System.out.println("Saldo: " + cuenta.getSaldo());
//            if (cuenta.getTipoCuenta().equalsIgnoreCase(CUENTA_AHORRO)) {
//                cuentaAhorro = (CuentaAhorroClass) util.getCuentaOfArrayList(i);
//                System.out.println("I.Anual: " + cuentaAhorro.getTipoInteresAnual());
//
//            }
//            if (cuenta.getTipoCuenta().equalsIgnoreCase(CUENTA_CORRIENTE_PERSONAL)) {
//                CCPersonal = (CuentaCorrientePersonalClass) util.getCuentaOfArrayList(i);
//                System.out.println("I.Mantenimiento: " + CCPersonal.getComisionMantenimiento());
//            }
//            if (cuenta.getTipoCuenta().equalsIgnoreCase(CUENTA_CORRIENTE_EMPRESA)) {
//                CCEmpresa = (CuentaCorrienteEmpresaClass) util.getCuentaOfArrayList(i);
//                System.out.println("Max Descubierto: " + CCEmpresa.getMaximoDescubiertoPermitido());
//                System.out.println("I.Descubierto: " + CCEmpresa.getTipoInteresDescubierto());
//            }
//            System.out.println("----------------------------");
//        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupCuentasCorrientes;
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonCrearCuenta;
    private javax.swing.JButton jButtonReset;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanelCAhorro;
    private javax.swing.JPanel jPanelCCEmpresa;
    private javax.swing.JPanel jPanelCCPersonal;
    private javax.swing.JPanel jPanelTipos;
    private javax.swing.JRadioButton jRadioButtonCAhorro;
    private javax.swing.JRadioButton jRadioButtonCCEmpresa;
    private javax.swing.JRadioButton jRadioButtonCCPersona;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField jTextFieldApellidos;
    private javax.swing.JTextField jTextFieldComisionFijaDescubierto;
    private javax.swing.JTextField jTextFieldDni;
    private javax.swing.JTextField jTextFieldFnacimiento;
    private javax.swing.JTextField jTextFieldInteresDescubierto;
    private javax.swing.JTextField jTextFieldMaximoDescubierto;
    private javax.swing.JTextField jTextFieldNCuenta;
    private javax.swing.JTextField jTextFieldNombre;
    private javax.swing.JTextField jTextFieldSaldoInicial;
    private javax.swing.JTextField jTextFieldTIMantenimiento;
    private javax.swing.JTextField jTextFieldTIRemuneracion;
    // End of variables declaration//GEN-END:variables
}
