package negocio;

/**
 *
 * @author Abraham Fernandez Sacnhez
 */
public class CuentaAhorroClass extends CuentaClass{
    
    private Double tipoInteresAnual;

    public CuentaAhorroClass() {
    }   
    
    /**
     * Constructor de la clase CuentaAhorroClass
     * 
     * @param tipoInteresAnual Double
     * @param titular Object - Objeto de la clase PersonaClass
     * @param tipoCuenta String
     * @param saldo Double
     * @param numeroCuenta String  
     */
    public CuentaAhorroClass(Double tipoInteresAnual, PersonaClass titular, String tipoCuenta, Double saldo, String numeroCuenta) {
        super(titular, tipoCuenta, saldo, numeroCuenta);
        this.tipoInteresAnual = tipoInteresAnual;
    }

    public Double getTipoInteresAnual() {
        return tipoInteresAnual;
    }

    public void setTipoInteresAnual(Double tipoInteresAnual) {
        this.tipoInteresAnual = tipoInteresAnual;
    }
    
    

}
