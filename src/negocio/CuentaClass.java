package negocio;

/**
 *
 * @author Abraham Fernandez Sacnhez
 */
public class CuentaClass {

    private PersonaClass titular;
    private String tipoCuenta;
    private Double saldo;
    private String numeroCuenta;  

    /**
     * 
     */
    public CuentaClass() {
    }

    /**
     * Constructor general de la clase CuentaClass, que es una clase general para todas las cuentas
     * @param titular Object - Objeto de la clase PersonaClass
     * @param tipoCuenta String
     * @param saldo Double
     * @param numeroCuenta String 
     */
    public CuentaClass(PersonaClass titular, String tipoCuenta, Double saldo, String numeroCuenta) {
        this.titular = titular;
        this.tipoCuenta = tipoCuenta;
        this.saldo = saldo;
        this.numeroCuenta = numeroCuenta;
    }
    
    public PersonaClass getTitular() {
        return titular;
    }

    public void setTitular(PersonaClass titular) {
        this.titular = titular;
    }

    public Double
         getSaldo() {
        return saldo;
    }

    public void setSaldo(Double
            saldo) {
        this.saldo = saldo;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }
    
}
