/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package negocio;

import java.util.ArrayList;

/**
 *
 * @author Abraham Fernandez Sacnhez
 */
public class CuentaCorrienteClass extends CuentaClass {

    private ArrayList<String>listaEntidades;

    /**
     * 
     */
    public CuentaCorrienteClass() {
    }

    /**
     * Constructor de la clase CuentaCorrienteClass, que es general solo para las cuentas corrientes
     * @param listaEntidades ArrayList - Contiene un listado de entidades crediticias
     * @param titular Object - Objeto de la clase PersonaClass
     * @param tipoCuenta String
     * @param saldo Double
     * @param numeroCuenta String
     */
    public CuentaCorrienteClass(ArrayList<String>listaEntidades, PersonaClass titular, String tipoCuenta, Double saldo, String numeroCuenta) {
        super(titular, tipoCuenta, saldo, numeroCuenta);
        this.listaEntidades = listaEntidades;
    }

    public ArrayList<String> getListaEntidades() {
        return listaEntidades;
    }
    
    
    
}
