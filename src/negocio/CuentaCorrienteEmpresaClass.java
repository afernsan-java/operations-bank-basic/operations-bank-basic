/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package negocio;

import java.util.ArrayList;

/**
 *
 * @author Abraham Fernandez Sacnhez
 */
public class CuentaCorrienteEmpresaClass extends CuentaCorrienteClass{

        private Double tipoInteresDescubierto;
        private Double maximoDescubiertoPermitido;
        private Double ComisionFijaDescubierto;

    public CuentaCorrienteEmpresaClass() {
    }   

    public CuentaCorrienteEmpresaClass(Double tipoInteresDescubierto, Double maximoDescubiertoPermitido, Double ComisionFijaDescubierto, ArrayList<String> listaEntidades, PersonaClass titular, String tipoCuenta, Double saldo, String numeroCuenta) {
        super(listaEntidades, titular, tipoCuenta, saldo, numeroCuenta);
        this.tipoInteresDescubierto = tipoInteresDescubierto;
        this.maximoDescubiertoPermitido = maximoDescubiertoPermitido;
        this.ComisionFijaDescubierto = ComisionFijaDescubierto;
    }

    public Double getTipoInteresDescubierto() {
        return tipoInteresDescubierto;
    }

    public void setTipoInteresDescubierto(Double tipoInteresDescubierto) {
        this.tipoInteresDescubierto = tipoInteresDescubierto;
    }

    public Double getMaximoDescubiertoPermitido() {
        return maximoDescubiertoPermitido;
    }

    public void setMaximoDescubiertoPermitido(Double maximoDescubiertoPermitido) {
        this.maximoDescubiertoPermitido = maximoDescubiertoPermitido;
    }

    public Double getComisionFijaDescubierto() {
        return ComisionFijaDescubierto;
    }

    public void setComisionFijaDescubierto(Double ComisionFijaDescubierto) {
        this.ComisionFijaDescubierto = ComisionFijaDescubierto;
    }
    
}
