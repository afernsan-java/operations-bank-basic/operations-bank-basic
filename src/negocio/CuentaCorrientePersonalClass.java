
package negocio;

import java.util.ArrayList;

/**
 *
 * @author Abraham Fernandez Sacnhez
 */
public class CuentaCorrientePersonalClass extends CuentaCorrienteClass{
    
    private Double comisionMantenimiento;

    /**
     * 
     */
    public CuentaCorrientePersonalClass() {
    }

    
    /**
     * Constructor de la clase CuentaCorrientePersonalClass
     * 
     * @param comisionMantenimiento Double
     * @param listaEntidades ArrayList - Contiene un listado de entidades crediticias
     * @param titular Object - Objeto de la clase PersonaClass
     * @param tipoCuenta String
     * @param saldo Double
     * @param numeroCuenta String
     */
    public CuentaCorrientePersonalClass(Double comisionMantenimiento, ArrayList<String> listaEntidades, PersonaClass titular, String tipoCuenta, Double saldo, String numeroCuenta) {
        super(listaEntidades, titular, tipoCuenta, saldo, numeroCuenta);
        this.comisionMantenimiento = comisionMantenimiento;
    }

    /**
     * Metodo Get de la comision de mantenimiento
     * @return Double
     */
    public Double getComisionMantenimiento() {
        return comisionMantenimiento;
    }

    /**
     * Metodo Set de la comision de mantenimiento
     * @param comisionMantenimiento Double
     */
    public void setComisionMantenimiento(Double comisionMantenimiento) {
        this.comisionMantenimiento = comisionMantenimiento;
    }

    

}
