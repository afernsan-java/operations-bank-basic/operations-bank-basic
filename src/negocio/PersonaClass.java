package negocio;

/**
 *
 * @author Abraham Fernandez Sacnhez
 */
public class PersonaClass {

    private String nombre;
    private String apellidos;
    private String fechaNacimiento;
    private String dni;

    /**
     * Constructor de la clase PersonaClass
     * @param nombre String
     * @param apellidos String
     * @param fechaNacimiento String
     * @param dni String
     */
    public PersonaClass(String nombre, String apellidos, String fechaNacimiento, String dni) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.fechaNacimiento = fechaNacimiento;
        this.dni = dni;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
    
    
    
}
