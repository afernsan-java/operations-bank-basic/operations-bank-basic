package negocio;

import static java.lang.Integer.parseInt;
import java.util.ArrayList;

/**
 *
 * @author Abraham Fernandez Sacnhez
 */
public class UtilClass {

    PersonaClass persona;
//    CuentaClass cuenta;
    CuentaClass cuenta;
    CuentaAhorroClass cuentaAhorro;
    CuentaCorrientePersonalClass ccPersonal;
    CuentaCorrienteEmpresaClass ccEmpresa;

    // Declaracion de variables    
    String nombre;
    String apellidos;
    String fechaNacimiento;
    String dni;

    int saldo;
    String numeroCuenta;
    int tipoInteres;
    static ArrayList<String> listaEntidades;
    int comisionMantenimiento;
    int tipoInteresDescubierto;
    int maximoDescubiertoPermitido;

    static ArrayList<Object> arrayListCuentas = new ArrayList<>();

    
    /**
     * Metodo que devolvera las entidades por defecto a las lista de entidades
     * autorizadas
     *
     * @return ArrayList listaEntidades
     */
    public ArrayList entidadesAutorizadas() {
        listaEntidades = new ArrayList<>();
        listaEntidades.add("Santander");
        listaEntidades.add("BBVA");
        listaEntidades.add("Bankia");
        listaEntidades.add("Sabadel");
        return listaEntidades;
    }

    /**
     * Metodo para a�adir un nuevo objeto al ArrayList
     *
     * @param cuenta Objeto de la clase CuentaClass
     */
    public void addNewCuenta(Object cuenta) {
        arrayListCuentas.add(cuenta);
    }

    /**
     * Metodo devolver el objeto de una posicion del ArrayList
     *
     * @param posicion Integer numero de la posicion dentro del array que se
     * quiere utilizar
     * @return Objeto cuenta de la clase CuentaClass
     */
    public Object getCuentaOfArrayList(int posicion) {
        return arrayListCuentas.get(posicion);
    }

    /**
     * Metodo que devuelve el numero total de posiciones utilizadas en el
     * arrayList
     *
     * @return Integer numero total de objetos dentro del arrayList
     */
    public int getSizeArrayCuentas() {
        return arrayListCuentas.size();
    }

    /**
     * Metodo para recuperar una cuenta por medio de su numero de cuenta
     * asociado
     *
     * @param nCuenta String numero de cuenta
     * @return Object objeto que contiene un objeto de una de las 3 clases
     * diferentes de cuentas
     */
    public Object getCuenta(String nCuenta) {
        int sizeCuentas = getSizeArrayCuentas();
        Object cuentaSeleccionada;
        for (int i = 0; i < sizeCuentas; i++) {
            cuentaSeleccionada = getCuentaOfArrayList(i);
            String claseDeCuenta = cuentaSeleccionada.getClass().getSimpleName();
            switch (claseDeCuenta) {
                case "CuentaAhorroClass":
                    cuentaAhorro = (CuentaAhorroClass) cuentaSeleccionada;
                    numeroCuenta = cuentaAhorro.getNumeroCuenta();
                    if (numeroCuenta.equalsIgnoreCase(nCuenta)) return cuentaAhorro;
                case "CuentaCorrientePersonalClass":
                    ccPersonal = (CuentaCorrientePersonalClass) cuentaSeleccionada;
                    numeroCuenta = ccPersonal.getNumeroCuenta();
                    if (numeroCuenta.equalsIgnoreCase(nCuenta)) return ccPersonal;  
                case "CuentaCorrienteEmpresaClass":
                    ccEmpresa = (CuentaCorrienteEmpresaClass) cuentaSeleccionada;
                    numeroCuenta = ccEmpresa.getNumeroCuenta();
                    if (numeroCuenta.equalsIgnoreCase(nCuenta)) return ccEmpresa; 
                default:
                    break;
            }
        }
        return null;
    }

    /**
     * Metodo para ingresar un nuevo saldo a una cuenta
     *
     * @param nCuenta String
     * @param saldoAIngresar String
     */
    public void ingresarSaldo(String nCuenta, String saldoAIngresar) {     
        Double ingreso = Double.parseDouble(saldoAIngresar);
        int sizeArrayCuentas = getSizeArrayCuentas();
        for (int i = 0; i < sizeArrayCuentas; i++) {
            Object cuentaObj = getCuentaOfArrayList(i);
            switch (getTextTipoClaseCuenta(cuentaObj)) {
                case "CuentaAhorroClass":
                    cuentaAhorro = (CuentaAhorroClass) cuentaObj;
                    if (nCuenta.equals(cuentaAhorro.getNumeroCuenta())) {
                        eraseObjeto(i);  
                        Double saldoActual = cuentaAhorro.getSaldo();
                        cuentaAhorro.setSaldo(sumarSaldo(saldoActual, ingreso));
                        addNewCuenta(cuentaAhorro);
                    }
                    break;
                case "CuentaCorrientePersonalClass":
                    ccPersonal = (CuentaCorrientePersonalClass) cuentaObj;
                    if (nCuenta.equals(ccPersonal.getNumeroCuenta())) {
                        eraseObjeto(i);
                        Double saldoActual = ccPersonal.getSaldo();
                        ccPersonal.setSaldo(sumarSaldo(saldoActual, ingreso));
                        addNewCuenta(ccPersonal);
                    }
                    break;
                case "CuentaCorrienteEmpresaClass":
                    ccEmpresa = (CuentaCorrienteEmpresaClass) cuentaObj;
                    if (nCuenta.equals(ccEmpresa.getNumeroCuenta())) {
                        eraseObjeto(i);
                        Double saldoActual = ccEmpresa.getSaldo();
                        ccEmpresa.setSaldo(sumarSaldo(saldoActual, ingreso));
                        addNewCuenta(ccEmpresa);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Metodo para retirar efectivo de una cuenta
     *
     * @param nCuenta String
     * @param efectivoRetirar String
     * @return Boolean
     */
    public Boolean retirarEfectivo(String nCuenta, String efectivoRetirar) {   
        Double retiro = Double.parseDouble(efectivoRetirar);
        int sizeArrayCuentas = getSizeArrayCuentas();
        for (int i = 0; i < sizeArrayCuentas; i++) {
            Object cuentaObj = getCuentaOfArrayList(i);
            switch (getTextTipoClaseCuenta(cuentaObj)) {
                case "CuentaAhorroClass":
                    eraseObjeto(i);
                    return sacarEfectivoDeCuentaAhorro(cuentaObj, nCuenta, efectivoRetirar);
                case "CuentaCorrientePersonalClass":
                    eraseObjeto(i);
                    return sacarEfectivoDeCuentaPersonal(cuentaObj, nCuenta, efectivoRetirar);
                case "CuentaCorrienteEmpresaClass":
                    eraseObjeto(i);
                    return sacarEfectivoDeCuentaEmpresa(cuentaObj, nCuenta, efectivoRetirar);
                default:
                    break;
            }
        }
        return true;
    }

    /**
     * Metodo para retirar efectivo de las cuentas Ahorro
     *
     * @param cuentaObj Object objeto de la clase CuentaAhorroClass
     * @param nCuenta String Numero de la cuenta bancaria
     * @param efectivoRetirar String cantidad de efectivo a retirar
     * @return Boolean retornara dependiendo de si queda saldo disponible
     */
    private boolean sacarEfectivoDeCuentaAhorro(Object cuentaObj, String nCuenta, String efectivoRetirar) {
        Double saldoResultado;
        Double retiro = Double.parseDouble(efectivoRetirar);
        cuentaAhorro = (CuentaAhorroClass) cuentaObj;
        if (nCuenta.equals(cuentaAhorro.getNumeroCuenta())) {
            saldoResultado = restarSaldo(cuentaAhorro.getSaldo(), retiro);
            if (!isSaldoNegativo(saldoResultado)) {
                cuentaAhorro.setSaldo(saldoResultado);
                addNewCuenta(cuentaAhorro);
                return true;
            }
        }
        addNewCuenta(cuentaAhorro);
        return false;
    }

    /**
     * Metodo para retirar efectivo de las cuentas Personales
     *
     * @param cuentaObj Object objeto de la clase CuentaAhorroClass
     * @param nCuenta String Numero de la cuenta bancaria
     * @param efectivoRetirar String cantidad de efectivo a retirar
     * @return Boolean retornara dependiendo de si queda saldo disponible
     */
    private boolean sacarEfectivoDeCuentaPersonal(Object cuentaObj, String nCuenta, String efectivoRetirar) {
        Double saldoResultado;
        Double retiro = Double.parseDouble(efectivoRetirar);
        ccPersonal = (CuentaCorrientePersonalClass) cuentaObj;
        if (nCuenta.equals(ccPersonal.getNumeroCuenta())) {
            saldoResultado = restarSaldo(ccPersonal.getSaldo(), retiro);
            if (!isSaldoNegativo(saldoResultado)) {
                ccPersonal.setSaldo(saldoResultado);
                addNewCuenta(ccPersonal);
                return true;
            }
        }
        addNewCuenta(ccPersonal);
        return false;
    }

    /**
     * Metodo para retirar efectivo de las cuentas Empresa
     *
     * @param cuentaObj Object objeto de la clase CuentaAhorroClass
     * @param nCuenta String Numero de la cuenta bancaria
     * @param efectivoRetirar String cantidad de efectivo a retirar
     * @return Boolean retornara dependiendo de si queda saldo disponible
     */
    private boolean sacarEfectivoDeCuentaEmpresa(Object cuentaObj, String nCuenta, String efectivoRetirar) {
        Double saldoResultado;
        Double retiro = Double.parseDouble(efectivoRetirar);
        ccEmpresa = (CuentaCorrienteEmpresaClass) cuentaObj;
        if (nCuenta.equals(ccEmpresa.getNumeroCuenta())) {
            saldoResultado = restarSaldo(ccEmpresa.getSaldo(), retiro);
            if (!isSaldoNegativo(saldoResultado)) {
                ccEmpresa.setSaldo(saldoResultado);
                addNewCuenta(ccEmpresa);
                return true;
            } else {
                if (isDescubiertoPermitido(retiro, ccEmpresa)) {
                    ccEmpresa.setSaldo(saldoResultado);
                    addNewCuenta(ccEmpresa);
                    return true;
                }
            }
        }
        addNewCuenta(ccEmpresa);
        return false;
    }

    /**
     * Metodo que nos dice si el saldo introducido es negativo o no
     *
     * @param saldoActual Double
     * @return Boolean
     */
    private Boolean isSaldoNegativo(Double saldoActual) {
        return saldoActual < 0;
    }

    /**
     * Metodo para saber si se supera el maximo descubierto permitido
     *
     * @param importeRetiro Double cantidad a retirar del saldo
     * @param ccEmpresa Objeto de la clase CuentaCorrienteEmpresaClass
     * @return Boolean
     */
    private Boolean isDescubiertoPermitido(Double importeRetiro, CuentaCorrienteEmpresaClass ccEmpresa) {
        Double saldoActual = ccEmpresa.getSaldo();
        Double maxDescubiertoPermitido = ccEmpresa.getMaximoDescubiertoPermitido();
        return saldoActual + maxDescubiertoPermitido >= importeRetiro;
    }

    /**
     * Metodo para sumar el retiro de un nuevo saldo
     *
     * @param saldoActual Double Cantidad de saldo actualmente
     * @param saldoAIngresar Double cantidad de saldo que se quiere ingresar en
     * la cuenta
     * @return Double
     */
    private Double sumarSaldo(Double saldoActual, Double saldoAIngresar) {
        return saldoActual + saldoAIngresar;
    }

    /**
     * Metodo para restar un importe al sado actual
     *
     * @param saldoActual Double
     * @param saldoARetirar Double
     * @return
     */
    private Double restarSaldo(Double saldoActual, Double saldoARetirar) {
        return saldoActual - saldoARetirar;
    }

    /**
     * Metodo que borra una cuenta del array por medio de el numero de cuenta
     * asociado
     *
     * @param nCuenta String
     */
    public void deleteCuenta(String nCuenta) {
        int sizeArrayCuentas = getSizeArrayCuentas();
        for (int i = 0; i < sizeArrayCuentas; i++) {
            Object cuentaObj = getCuentaOfArrayList(i);
            switch (getTextTipoClaseCuenta(cuentaObj)) {
                case "CuentaAhorroClass":
                    cuentaAhorro = (CuentaAhorroClass) cuentaObj;
                    if (nCuenta.equals(cuentaAhorro.getNumeroCuenta())) {
                        eraseObjeto(i);
                    }
                    break;
                case "CuentaCorrientePersonalClass":
                    ccPersonal = (CuentaCorrientePersonalClass) cuentaObj;
                    if (nCuenta.equals(ccPersonal.getNumeroCuenta())) {
                        eraseObjeto(i);
                    }
                    break;
                case "CuentaCorrienteEmpresaClass":
                    ccEmpresa = (CuentaCorrienteEmpresaClass) cuentaObj;
                    if (nCuenta.equals(ccEmpresa.getNumeroCuenta())) {
                        eraseObjeto(i);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Metodo para el borrado de un objeto en la posicion del array introducida
     *
     * @param posicion Integer
     */
    private void eraseObjeto(int posicion) {
        arrayListCuentas.remove(posicion);
    }

    /**
     * Metodo que devuelve un String con el nombre de la clase del tipo de
     * cuenta
     *
     * @param cuentaObj Object
     * @return String
     */
    private String getTextTipoClaseCuenta(Object cuentaObj) {
        return cuentaObj.getClass().getSimpleName();
    }

    /**
     * Metodo para saber si el valor de un dato String puede ser un numero o no
     *
     * @param cadena String
     * @return Boolean
     */
    public boolean isNumeric(String cadena) {
        try {            
            Double.parseDouble(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    /**
     * COMPROBACION DE VALIDEZ LOS DIGITOS DE CONTROL para hacer la operacion
     * para saber si el codigo de los digitos de control es correcto, ire
     * cogiendo uno a uno en el orden que marque todos los elementos del string
     * en el cual tengo metido el numero de cuenta, antes de poder realizar
     * operaciones matematicas utilizare la funcion parseInt para convertir los
     * caracteres numericos en numeros enteros y asi poder realizar operaciones
     * matematicas con ellos. <br>
     * Numeros comprovacion: 
     * validado: 78535777435560788494
     * 
     * No validado: 44714539441838207677
     *              83880314760047874037
     *              52039445355892063978
     * @param numeroCuentaCompleta String numero de cuenta a comprobar
     * @return Boolean
     */
    private boolean ComprobacionDigitoControl(String numeroCuentaCompleta) {
        try {
            String numeroControl = "00" + numeroCuentaCompleta.substring(0, 20); 
            int suma_1 = parseInt(numeroControl.substring(0, 1)) * 1
                    + parseInt(numeroControl.substring(1, 2)) * 2
                    + parseInt(numeroControl.substring(2, 3)) * 4
                    + parseInt(numeroControl.substring(3, 4)) * 8
                    + parseInt(numeroControl.substring(4, 5)) * 5
                    + parseInt(numeroControl.substring(5, 6)) * 10
                    + parseInt(numeroControl.substring(6, 7)) * 9
                    + parseInt(numeroControl.substring(7, 8)) * 7
                    + parseInt(numeroControl.substring(8, 9)) * 3
                    + parseInt(numeroControl.substring(9, 10)) * 6;
            int primerDigitoControl = ((suma_1 % 11) - 11) * -1;       
            int suma_2 = parseInt(numeroControl.substring(12, 13)) * 1
                    + parseInt(numeroControl.substring(13, 14)) * 2
                    + parseInt(numeroControl.substring(14, 15)) * 4
                    + parseInt(numeroControl.substring(15, 16)) * 8
                    + parseInt(numeroControl.substring(16, 17)) * 5
                    + parseInt(numeroControl.substring(17, 18)) * 10
                    + parseInt(numeroControl.substring(18, 19)) * 9
                    + parseInt(numeroControl.substring(19, 20)) * 7
                    + parseInt(numeroControl.substring(20, 21)) * 3
                    + parseInt(numeroControl.substring(21, 22)) * 6;
            int segundoDigitoControl = ((suma_2 % 11) - 11) * -1;
            return primerDigitoControl == parseInt(numeroControl.substring(10, 11)) && segundoDigitoControl == parseInt(numeroControl.substring(11, 12));            
        } catch (StringIndexOutOfBoundsException e) {
            return false;
        }
    }

    /**
     * Metodo para validar que los datos introducidos son numericos y se
     * corresponden con la longitud deseada
     *
     * @param numeroCCC String
     * @return Boolean
     */
    private boolean validacionFormatoCCC(String numeroCCC) {
        System.out.println(numeroCCC.length());
        System.out.println(isNumeric(numeroCCC));        
        return numeroCCC.length() == 20 && isNumeric(numeroCCC);
    }
    
    /**
     * Metodo comprobador de validez tanto del formato como del control numerico del numero de una cuenta corriente
     * @param numeroCCC String - numero integro de una cuenta corriente
     * @return Boolean
     */
    public boolean validacionNumeroCuentaCorriente(String numeroCCC){            
            return ComprobacionDigitoControl(numeroCCC);            
    }

}
